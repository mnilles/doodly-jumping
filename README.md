# Doodly Jumping

This little demo game was developed with [Godot 3.0.6](https://godotengine.org/download/windows).  

## Additional setup steps

Please download the [Sprinklescolors font by Des](http://www.fontspace.com/des/sprinklescolors) and put `Sprinklescolors.ttf` into the `font` folder.  This file is not included because of licensing.